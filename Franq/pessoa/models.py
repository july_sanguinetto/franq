from django.db import models

# Create your models here.
class Pessoa(models.Model):
    nome = models.CharField('Nome', max_length=100)
    fone = models.CharField('Telefone', max_length=11)
    email = models.EmailField('Email')

    def __str__(self):
        return str(self.id)


