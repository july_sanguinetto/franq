from django.urls import path, include
from . import views


app_name = 'pessoa'

urlpatterns = [
    #path('cadastrodepessoa/', views.CadastrodePessoa, name='cadastrodepessoa'),
    path('/', views.CadastrodePessoa, name = 'CadastrodePessoa'),
    path('login_user/', views.login_user, name='login_user'),
    path('login_user/submit/', views.submit_login, name='submit_login'),
    path('cadastrodepessoa/', views.ApresentaPessoa, name='ApresentaPessoa'),
    path('pessoa/<int:id>/', views.pessoaView, name='pessoa'),
    path('cadastrodepessoa/novapessoa/', views.novaPessoa, name='novaPessoa')
]