from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth import authenticate, login
from django.contrib import messages
from .models import Pessoa
from .forms import PessoaForm

# Create your views here.
def CadastrodePessoa(request):
    return render(request, 'pessoa/cadastrodepessoa.html')


def login_user(request):
    return render(request, 'pessoa/login_user.html')

@csrf_protect
def submit_login(request):
    if request.POST:
        username=request.POST.get('username')
        password=request.POST.get('password')
        print(username)
        print(password)
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/login_user/#')
        else:
            messages.error(request, 'Usuário ou Senha inválido. Tente novamente.')
    return redirect('/login_user/')

def ApresentaPessoa(request):
    pessoas = Pessoa.objects.all()
    return render(request, 'pessoa/cadastrodepessoa.html', {'pessoas': pessoas})


def pessoaView(request, id):
    pessoa = get_object_or_404(Pessoa, pk=id)
    return render(request, 'pessoa/pessoa.html', {'pessoa': pessoa})

def novaPessoa(request):
    if request.method == "POST":
        form = PessoaForm(request.POST)

        if form.is_valid():
            pessoa = form.save()
            pessoa.save()
            return redirect(request, 'pessoa/cadastrodepessoa.html', {'form': form})
    else:
        form = PessoaForm()
        return render(request, 'pessoa/novaPessoa.html', {'form': form})