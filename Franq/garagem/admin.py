from django.contrib import admin
from .models import CadastrodeViculo
from .models import CadastroGaragem
from .models import GaragemVeiculo


# Register your models here.
@admin.register(CadastrodeViculo)
class carroAdmin(admin.ModelAdmin):
    list_display = ['id', 'tipoVeiculo']

# Register your models here.
@admin.register(CadastroGaragem)
class garagemAdmin(admin.ModelAdmin):
    list_display = ['id', 'nomeGaragem']

# Register your models here.
@admin.register(GaragemVeiculo)
class garagemVeiculoAdmin(admin.ModelAdmin):
    list_display = ['id','IdGaragem', 'IdVeiculo']