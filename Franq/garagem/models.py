from django.db import models

# Create your models here.
class CadastrodeViculo(models.Model):
    cor = models.CharField('Cor do Veículo', max_length=100)
    anoFabrica = models.CharField('Ano de Fabricação', max_length=100)
    modelo = models.CharField('Modelo do veículo', max_length=100)
    tipoVeiculo = models.CharField('tipo do veículo', max_length=100)

    def __str__(self):
        return str(self.id)


class CadastroGaragem(models.Model):
    nomeGaragem = models.CharField('Nome da Garagem', max_length=100)

    def __str__(self):
        return str(self.id)


class GaragemVeiculo(models.Model):
    IdGaragem = models.ForeignKey(CadastroGaragem, on_delete=models.CASCADE)
    IdVeiculo = models.ForeignKey(CadastrodeViculo, on_delete=models.CASCADE, unique=True)
    unique_together = ('IdGaragem', 'IdVeiculo')

    def __str__(self):
        return str(self.id)