# Generated by Django 3.2.4 on 2021-06-20 23:54

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CadastrodeViculo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cor', models.CharField(max_length=100, verbose_name='Cor do Veículo')),
                ('anoFabrica', models.CharField(max_length=100, verbose_name='Ano de Fabricação')),
                ('modelo', models.CharField(max_length=100, verbose_name='Modelo do veículo')),
                ('tipoVeiculo', models.CharField(max_length=100, verbose_name='tipo do veículo')),
            ],
        ),
        migrations.CreateModel(
            name='CadastroGaragem',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nomeGaragem', models.CharField(max_length=100, verbose_name='Nome da Garagem')),
            ],
        ),
    ]
